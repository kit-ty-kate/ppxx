(* PPX related tools *)

open Utils
open Migrate_parsetree
   
let handle_error f =
  try f () with 
  | Syntaxerr.Error e ->
      !!% "%a@." Syntaxerr.report_error e;
      exit 2
  | e ->
      Format.eprintf "%a@."  Location.report_exception e;
      exit 2
      
module Make(A : sig 
  val name : string
  val options : (Arg.key * Arg.spec * Arg.doc) list
  val make_mapper : Driver.config -> Driver.cookies -> Ast_404.Ast_mapper.mapper
end) = struct
  open A
     
  let register () = 
    Driver.register
      ~name
      ~args:A.options
      Versions.ocaml_404
      make_mapper
  
  let legacy_main () = 
    register ();
    Driver.run_as_ppx_rewriter ()

end
